<?php
$installer = $this;
$installer->startSetup();


$installer->run("
DROP TABLE IF EXISTS {$this->getTable('mavitech_product_history')};
CREATE TABLE {$this->getTable('mavitech_product_history')} (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`product_id` INT(11) UNSIGNED NOT NULL,
	`date_update` DATETIME NOT NULL,
	`admin_name` TEXT NOT NULL,
	`ip_address` VARCHAR(15) NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `id` (`id`)
)
COMMENT='mavitech_product_history'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=24
;


");

$installer->endSetup();
