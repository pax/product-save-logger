<?php class Mavitech_Productsavelogger_Block_Adminhtml_Catalog_Product_Edit_Tab
    extends Mage_Adminhtml_Block_Widget_Grid implements Mage_Adminhtml_Block_Widget_Tab_Interface
{

    public function getTabLabel(){
            return Mage::helper('catalog')->__('Update History');
    }

    public function getTabTitle(){
        return Mage::helper('catalog')->__('Update History');
    }

    public function canShowTab(){
        return Mage::getSingleton('admin/session')->isAllowed('catalog/products/productsavelogger');
    }

    public function isHidden(){
        return false;
    }

    public function __construct()
    {
        parent::__construct();
        $this->setId('id');
        $this->setDefaultSort('update_time');
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('productsavelogger/history')->getCollection()->addFilter('product_id', Mage::registry('current_product')->getId());
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('date_update', array(
            'header'    => Mage::helper('catalog')->__('Update Time'),
            'filter'   => false,
            'sortable' => false,
            'width'     => '50px',
            'type'      => 'datetime',
            'index'     => 'date_update'
        ));

        $this->addColumn('admin_name', array(
            'header'    => Mage::helper('catalog')->__('Admin Name'),
            'filter'   => false,
            'sortable' => false,
            'width'     => '150px',
            'type'      => 'text',
            'index'     => 'admin_name'
        ));

        $this->addColumn('ip_address', array(
            'header'   => Mage::helper('catalog')->__('IP Address'),
            'index'     => 'ip_address',
            'filter'   => false,
            'sortable' => false,
            'width'    => '100',
            'type'     => 'ip'
        ));

        return parent::_prepareColumns();
    }

}