<?php
class Mavitech_ProductSaveLogger_Model_Resource_History extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('productsavelogger/history', 'id');
    }
}