<?php
class Mavitech_ProductSaveLogger_Model_Observer extends Varien_Object
{
    public function saveLog($observer)
    {
        try {
            $data = array(
                'product_id'    => $this->getProductId($observer),
                'admin_name'    => $this->getAdminName(),
                'date_update'   => $this->getCurrentTime(),
                'ip_address'    => $this->getUserIp()
            );
            $model = Mage::getModel('productsavelogger/history')->setData($data);
            $model->save();
        } catch (Exception $e){
            echo $e->getMessage();
        }
    }

    public function getProductId($observer) {
        return $observer->getEvent()->getProduct()->getId();
    }

    public function getAdminName() {
        return Mage::getSingleton('admin/session')->getUser()->getName();
    }

    public function getUserIp() {
        return Mage::helper('core/http')->getRemoteAddr(false);
    }

    public function getCurrentTime() {
        return Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');
    }

}
